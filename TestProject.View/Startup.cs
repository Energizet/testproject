using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestProject.Domain.Controllers;
using TestProject.Domain.Controllers.Interfaces;
using IDbGameController = TestProject.DB.IGameController;
using DbGameController = TestProject.DB.GameController;
using TestProject.DB.Controllers;
using TestProject.DB.Controllers.Interfaces;
using LightInject;

namespace TestProject.View
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			var container = new ServiceContainer();
			container.Register<IGameDbController, GameDbController>();
			container.Register<IDeveloperDbController, DeveloperDbController>();
			container.Register<IGenreDbController, GenreDbController>();
			container.Register<IDbGameController, DbGameController>();
			container.Register<IGameController, GameController>();

			services.AddSingleton(_ => container.GetInstance<IGameDbController>());
			services.AddSingleton(_ => container.GetInstance<IDeveloperDbController>());
			services.AddSingleton(_ => container.GetInstance<IGenreDbController>());
			services.AddSingleton(_ => container.GetInstance<IDbGameController>());
			services.AddSingleton(_ => container.GetInstance<IGameController>());

			services.AddControllers();
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "TestProject.View", Version = "v1" });
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseSwagger();
				app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TestProject.View v1"));
			}

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
